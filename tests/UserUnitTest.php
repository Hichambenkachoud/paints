<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIfTrue(): void
    {
        $user = new User();
        $user->setEmail('email@email.com')
            ->setFirstName('firstname')
            ->setLastName('lastname')
            ->setPassword('password')
            ->setPhoneNumber('212600000000')
            ->setInstagram('instagram')
            ->setAbout('about us');

        $this->assertEquals('email@email.com', $user->getEmail());
        $this->assertEquals('firstname', $user->getFirstName());
        $this->assertEquals('lastname', $user->getLastName());
        $this->assertEquals('password', $user->getPassword());
        $this->assertEquals('212600000000', $user->getPhoneNumber());
        $this->assertEquals('instagram', $user->getInstagram());
        $this->assertEquals('about us', $user->getAbout());

    }
    public function testIfFalse(): void
    {
        $user = new User();
        $user->setEmail('email@email.com')
            ->setFirstName('firstname')
            ->setLastName('lastname')
            ->setPassword('password')
            ->setPhoneNumber('212600000000')
            ->setInstagram('instagram')
            ->setAbout('about us');

        $this->assertNotEquals('false@email.com', $user->getEmail());
        $this->assertNotEquals('false', $user->getFirstName());
        $this->assertNotEquals('false', $user->getLastName());
        $this->assertNotEquals('false', $user->getPassword());
        $this->assertNotEquals('false', $user->getPhoneNumber());
        $this->assertNotEquals('false', $user->getInstagram());
        $this->assertNotEquals('false', $user->getAbout());

    }
}
