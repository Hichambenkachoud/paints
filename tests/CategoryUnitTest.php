<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIfTrue(): void
    {
        $category = new Category();
        $category->setName('name')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertEquals('name', $category->getName());
        $this->assertEquals('description', $category->getDescription());
        $this->assertEquals('slug', $category->getSlug());
    }
    public function testIfFalse(): void
    {
        $category = new Category();
        $category->setName('name')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertNotEquals('false', $category->getName());
        $this->assertNotEquals('false', $category->getDescription());
        $this->assertNotEquals('false', $category->getSlug());
    }
}
