# Paints

Site internet présentants des peintures

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony Cli
* Docker
* Docker-compose

### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```

### Lncer les tests

```bash
php bin/phpunit
```
