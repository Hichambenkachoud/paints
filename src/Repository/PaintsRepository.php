<?php

namespace App\Repository;

use App\Entity\Paints;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Paints|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paints|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paints[]    findAll()
 * @method Paints[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaintsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Paints::class);
    }

    // /**
    //  * @return Paints[] Returns an array of Paints objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Paints
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
